## Running web server with node

Install requirements for tiny web server

```
npm install
```

Start web server

```
node server.js
```

Open [localhost:8080](http://localhost:8080)